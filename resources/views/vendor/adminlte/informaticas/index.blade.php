@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Materiales Informatico</h1>
@stop

@section('content')


<div class="row">

 
 
 <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>nombre</th>
                <th>MAC</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $informaticas as $informatica )
            <tr>
                <td><a href="{{ url('/informaticas/show/' . $informatica->id ) }}"> {{$informatica->id }}</a></td>
                <td>{{$informatica->nombre }}</td>
                <td> {{$informatica->MAC }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>

 {{$informaticas->links()}}

</div>
@stop