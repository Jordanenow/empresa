@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Miembros</h1>
@stop

@section('content')


<div class="row">


 
 <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Empleado</th>
                <th>Departamento</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $miembros as $miembro )
            <tr>
                <td><a href="{{ url('/miembros/show/' . $miembro->id ) }}"> {{$miembro->id }}</a></td>
                <td>{{$miembro->name }}</td>
                <td>{{$miembro->nombre }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>

 {{$miembros->links()}}


</div>





@stop