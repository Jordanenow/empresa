@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Departamentos</h1>
@stop

@section('content')


<div class="row">


<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>ID</th> <th>Opciones</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $departamentos as $departamento )
            <tr>
                <td>{{$departamento->nombre }}</td>
                <td><a href="{{ url('/departamentos/show/' . $departamento->id ) }}"> {{$departamento->id }}</a></td>
                <td>
                <a class="btn btn-warning" href= "{{url('departamentos/edit/'.$departamento->id)}}" >Editar</a>
<form method="POST" action="{{url('/departamentos/delete').'/'.$departamento->id}}" style="display:inline">

 @method('DELETE')

 @csrf

 <button type="submit" class="btn btn-danger" role="button">Borrar</button>

</form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

 

 {{$departamentos->links()}}

 
@stop