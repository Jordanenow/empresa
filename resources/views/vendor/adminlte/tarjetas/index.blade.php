@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Tarjetas</h1>
@stop

@section('content')


<div class="row">



 <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>codigo</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $tarjetas as $tarjeta )
            <tr>
                <td><a href="{{ url('/tarjetas/show/' . $tarjeta->id ) }}"> {{$tarjeta->id }}</a></td>
                <td>{{$tarjeta->codigo }}</td>
               
            </tr>
            @endforeach
            </tbody>
        </table>

 {{$tarjetas->links()}}

</div>
@stop