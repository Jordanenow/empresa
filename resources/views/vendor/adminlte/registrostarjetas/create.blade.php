@extends('adminlte::page')

@section('title', 'WildWork')




@section('content')


<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Añadir Registro
         </div>
         <div class="card-body" style="padding:30px">

            {{-- TODO: Abrir el formulario e indicar el método POST --}}
            <form action="" method="post" enctype="multipart/form-data">
               {{-- TODO: Protección contra CSRF --}}
               @csrf
               @method('POST')
               <div class="form-group">
                  <label for="users">Usuario</label>
                  <select name="users" id="users">
                  @foreach($users as $key => $user)
                     
                     <option value="{{$user -> id}}"> {{$user -> name}}</option>
                    
                  @endforeach
                  </select>
               </div>

               <div class="form-group">
                  <label for="tarjetas">Tarjetas</label>
                  <select name="tarjetas" id="departatarjetasmentos">
                  @foreach($tarjetas as $key => $tarjeta)
                     
                     <option value="{{$tarjeta -> id}}"> {{$tarjeta -> codigo}}</option>
                    
                  @endforeach
                  </select>
               </div>

               <div class="form-group text-center">
                  <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                     Registrar tarjeta.
                  </button>
               </div>

            </form>
            {{-- TODO: Cerrar formulario --}}

         </div>
      </div>
   </div>
</div>
@stop