@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Registro de las tarjetas</h1>
@stop

@section('content')

<div class="row">


 
 <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Empleado</th>
                <th>Codigo</th>
                <th>Fecha</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $tregistros as $tregistro )
            <tr>
                <td><a href="{{ url('/registrostarjetas/show/' . $tregistro->id ) }}"> {{$tregistro->id }}</a></td>
                <td>{{$tregistro->name }}</td>
                <td>{{$tregistro->codigo }}</td>
                <th>{{$tregistro->created_at }}</th>
            </tr>
            @endforeach
            </tbody>
        </table>

 {{$tregistros->links()}}


</div>



@stop