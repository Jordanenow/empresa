@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Tu perfil</h1>
@stop

@section('content')


    <div class="row">


<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Opciones</th>
            </tr>
        </thead>

        <tbody>
            <tr>
          
                <td>{{$User->id }}</td>
                <td>{{$User->name }}</td>
                <td>{{$User->email }}</td>
                <td>
                <a class="btn btn-warning" href= "{{url('empleados/edit/'.$User->id)}}" >Editar el perfil</a>
<form method="POST" action="{{url('/empleados/delete').'/'.$User->id}}" style="display:inline">

 @method('DELETE')

 @csrf

 <button type="submit" class="btn btn-danger" role="button">Borrar</button>

</form>
                </td>

            </tr>
            </tbody>
        </table>

 


@stop