@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Registro de moviles</h1>
@stop

@section('content')




<div class="row">


 
 <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Empleado</th>
                <th>Marca</th>
                <th>Numero</th>
                <th>Fecha</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $mregistros as $mregistro )
            <tr>
                <td><a href="{{ url('/registrosmoviles/show/' . $mregistro->id ) }}"> {{$mregistro->id }}</a></td>
                <td>{{$mregistro->name }}</td>
                <td>{{$mregistro->marca }}</td>
                <td>{{$mregistro->numero }}</td>
                <th>{{$mregistro->created_at }}</th>
            </tr>
            @endforeach
            </tbody>
        </table>

 {{$mregistros->links()}}


</div>
@stop