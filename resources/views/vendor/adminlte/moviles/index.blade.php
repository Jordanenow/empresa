@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Moviles</h1>
@stop

@section('content')


<div class="row">

 
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Numero</th>
                <th>Marca</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $moviles as $movil )
            <tr>
                <td><a href="{{ url('/moviles/show/' . $movil->id ) }}"> {{$movil->id }}</a></td>
                <td>{{$movil->numero }}</td>
                <td> {{$movil->marca }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>

 {{$moviles->links()}}

</div>
@stop