@extends('adminlte::page')

@section('title', 'WildWork')




@section('content')

<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Añadir Movil
         </div>
         <div class="card-body" style="padding:30px">

            {{-- TODO: Abrir el formulario e indicar el método POST --}}
            <form action="" method="post" enctype="multipart/form-data">
            {{-- TODO: Protección contra CSRF --}}
            @csrf
           
            @method('POST')



            <div class="form-group">
            <label for="numero">Numero Telefonico</label>
               <input type="text" name="numero" id="numero" class="form-control" value="">
            </div>
            <div class="form-group">
            <label for="marca">Marca del Telefono</label>
               <input type="text" name="marca" id="marca" class="form-control" value="">
            </div>
            <div class="form-group">
            <label for="cuenta_google">Cuenta de Google</label>
               <input type="text" name="cuenta_google" id="cuenta_google" class="form-control" value="">
            </div>
            </div>
           
            <div class="form-group text-center">
               <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                   Crear Movil
               </button>
            </div>
            
            </form>
            {{-- TODO: Cerrar formulario --}}

         </div>
      </div>
   </div>
</div>
@stop