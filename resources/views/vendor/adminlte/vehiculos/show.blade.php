@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Vehiculos</h1>
@stop

@section('content')

    <h1>{{$Vehiculos -> modelo}}</h1>

    <div class="container">
    <div class="row">

<p> Su matricula: {{$Vehiculos -> matricula}} </p>

<a class="btn btn-warning" href= "{{url('vehiculos/edit/'.$Vehiculos->id)}}" >Editar</a>
<form method="POST" action="{{url('/vehiculos/delete').'/'.$Vehiculos->id}}" style="display:inline">

 @method('DELETE')

 @csrf

 <button type="submit" class="btn btn-danger" role="button">

 Borrar

 </button>

</form>
</div>


</div>
    </div>

@stop