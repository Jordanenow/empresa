@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Vehiculos</h1>
@stop

@section('content')



 

 <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Matricula</th>
                <th>Modelo</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $vehiculos as $vehiculo )
            <tr>
                <td><a href="{{ url('/vehiculos/show/' . $vehiculo->id ) }}"> {{$vehiculo->id }}</a></td>
                <td>{{$vehiculo->matricula }}</td>
                <td>{{$vehiculo->modelo }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>

 {{$vehiculos->links()}}


@stop