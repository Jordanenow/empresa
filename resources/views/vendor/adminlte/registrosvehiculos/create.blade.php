@extends('adminlte::page')

@section('title', 'WildWork')




@section('content')


<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
           
         </div>
         <div class="card-body" style="padding:30px">

            {{-- TODO: Abrir el formulario e indicar el método POST --}}
            <form action="" method="post" enctype="multipart/form-data">
               {{-- TODO: Protección contra CSRF --}}
               @csrf
               @method('POST')
               <div class="form-group">
                  <label for="users">Usuario</label>
                  <select name="users" id="users">
                  @foreach($users as $key => $user)
                     
                     <option value="{{$user -> id}}"> {{$user -> name}}</option>
                    
                  @endforeach
                  </select>
               </div>
               <div class="form-group">
                  <label for="vehiculos">Vehiculo</label>
                  <select name="vehiculos" id="vehiculos">
                  @foreach($vehiculos as $key => $vehiculo)
                     
                     <option value="{{$vehiculo -> id}}"> {{$vehiculo -> matricula}}</option>
                    
                  @endforeach
                  </select>
               </div>

               <div class="form-group text-center">
                  <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                     Asignar un Vehiculo.
                  </button>
               </div>

            </form>
            {{-- TODO: Cerrar formulario --}}

         </div>
      </div>
   </div>
</div>
@stop