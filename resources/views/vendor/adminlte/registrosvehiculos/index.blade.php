@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Registro de vehiculos</h1>
@stop

@section('content')



<div class="row">


 
 <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Empleado</th>
                <th>Modelo</th>
                <th>Matricula</th>
                <th>Fecha</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $vregistros as $vregistro )
            <tr>
                <td><a href="{{ url('/registrosvehiculos/show/' . $vregistro->id ) }}"> {{$vregistro->id }}</a></td>
                <td>{{$vregistro->name }}</td>
                <td>{{$vregistro->modelo }}</td>
                <td>{{$vregistro->matricula }}</td>
                <th>{{$vregistro->created_at }}</th>
            </tr>
            @endforeach
            </tbody>
        </table>

 {{$vregistros->links()}}


</div>




@stop