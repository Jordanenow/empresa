@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Usuarios</h1>
@stop

@section('content')

    <h1>{{$User -> name}}</h1>

    <div class="container">
    <div class="row">



<div class="col-sm-4">


<!-- <p>{{$User -> imagen}}</p> -->
<p> Su correo:{{$User -> email}}</p>

</div>

<div class="col-sm-8">


<p>El id: {{$User -> id}}</p>
<!-- <p>{{$User -> correo}}</p> -->

<a class="btn btn-warning" href= "{{url('empleados/edit/'.$User->id)}}" >Editar el perfil</a>
<form method="POST" action="{{url('/empleados/delete').'/'.$User->id}}" style="display:inline">

 @method('DELETE')

 @csrf

 <button type="submit" class="btn btn-danger" role="button">

 Borrar

 </button>

</form>
</div>


</div>
    </div>

@stop