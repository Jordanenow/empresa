@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Materiales</h1>
@stop

@section('content')


<div class="row">

 @foreach( $materiales as $material )

 <div class="col-xs-6 col-sm-4 col-md-3 text-center">

<a href="{{ url('/materiales/show/' . $material->id ) }}">  

 <!-- <img src="{url({$cliente->imagen})}" style="height:200px" /> -->

 <h4 style="min-height:45px;margin:5px 0 10px 0">

 {{$material->nombre }}

 </h4>

 </a>

 </div>

 @endforeach

 {{$materiales->links()}}

</div>
@stop