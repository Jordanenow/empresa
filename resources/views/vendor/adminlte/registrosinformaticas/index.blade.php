@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Registro del material informatico</h1>
@stop

@section('content')



<div class="row">


 
 <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Empleado</th>
                <th>Material</th>
                <th>Fecha</th>
            </tr>
        </thead>
        
        <tbody>
        @foreach( $iregistros as $iregistro )
            <tr>
                <td><a href="{{ url('registrosinformaticas/show/' . $iregistro->id ) }}"> {{$iregistro->id }}</a></td>
                <td>{{$iregistro->name }}</td>
                <td>{{$iregistro->nombre }}</td>
                <th>{{$iregistro->created_at }}</th>
            </tr>
            @endforeach
            </tbody>
        </table>

 {{$iregistros->links()}}


</div>



@stop