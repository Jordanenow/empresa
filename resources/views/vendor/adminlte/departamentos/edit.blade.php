@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Departamentos</h1>
@stop

@section('content')


<div class="row" style="margin-top:40px">
   <div class="offset-md-3 col-md-6">
      <div class="card">
         <div class="card-header text-center">
            Modificar Departamento
         </div>
         <div class="card-body" style="padding:30px">

            {{-- TODO: Abrir el formulario e indicar el método POST --}}
            <form action="" method="post" enctype="multipart/form-data">
               {{-- TODO: Protección contra CSRF --}}
               @csrf
               @method('PUT')
            

                  <label for="nombre">Nombre</label>
                  <input type="text" name="nombre" id="nombre" class="form-control" value="{{$departamento->nombre}}">
          
               <div class="form-group text-center">
                  <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                     Modificar Departamento
                  </button>
               </div>
            </form>
               {{-- TODO: Cerrar formulario --}}

         </div>
      </div>
   </div>
</div>

<form action="/foo/bar" method="POST">
   @method('PUT')

</form>

@stop