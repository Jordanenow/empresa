@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Departamentos</h1>
@stop

@section('content')

    <h1>{{$Departamento -> nombre}}</h1>

    <div class="container">
    <div class="row">





<div class="col-sm-8">


<p>El id: {{$Departamento -> id}}</p>


<a class="btn btn-warning" href= "{{url('departamentos/edit/'.$Departamento->id)}}" >Editar el perfil</a>
<form method="POST" action="{{url('/departamentos/delete').'/'.$Departamento->id}}" style="display:inline">

 @method('DELETE')

 @csrf

 <button type="submit" class="btn btn-danger" role="button">

 Borrar

 </button>

</form>
</div>


</div>
    </div>

@stop