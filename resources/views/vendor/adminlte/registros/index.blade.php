@extends('adminlte::page')

@section('title', 'WildWork')

@section('content_header')
    <h1>Registros</h1>
@stop

@section('content')


<div class="row">

 @foreach( $registros as $registro )

 <div class="col-xs-6 col-sm-4 col-md-3 text-center">

<a href="{{ url('/registro/show/' . $registro->id ) }}"> 


 <h4 style="min-height:45px;margin:5px 0 10px 0">

 El usuario {{$registro-> name }}, Pertenece al departamento {{$registro-> nombre }}.

 </h4>

 </a>

 </div>

 @endforeach

 {{$registros->links()}}

</div>



@stop