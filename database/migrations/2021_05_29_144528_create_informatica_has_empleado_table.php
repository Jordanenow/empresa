<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformaticaHasEmpleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informaticasregistradas', function (Blueprint $table) {
         
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('informatica_id');

            $table->timestamps(); 
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('informatica_id')->references('id')->on('materialesinformaticos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informaticasregistradas');
    }
}
