<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Departamento;
use Illuminate\Support\Facades\DB;

class DepartamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       /* DB::table('departamentos')->delete();
        Departamento::factory()->count(5)->create();
*/
        Departamento::create(['nombre'=>'Informática']);
        Departamento::create(['nombre'=>'Dirección']);
        Departamento::create(['nombre'=>'Administración']);
        Departamento::create(['nombre'=>'Contabilidad']);
        Departamento::create(['nombre'=>'Facturación']);
        Departamento::create(['nombre'=>'Proveedores']);
        Departamento::create(['nombre'=>'Economato']);
        Departamento::create(['nombre'=>'Restaurantes']);
        Departamento::create(['nombre'=>'Marketing']);
        Departamento::create(['nombre'=>'Comercial']);
        Departamento::create(['nombre'=>'Garden Center']);
        Departamento::create(['nombre'=>'Oficina Técnica']);
        Departamento::create(['nombre'=>'Recursos Humanos']);
        Departamento::create(['nombre'=>'Prevención']);
        Departamento::create(['nombre'=>'Arqueo']);
        Departamento::create(['nombre'=>'Cajeros']);
       

    }
}
