<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DepartamentoController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\InformaticaController;
use App\Http\Controllers\MovilController;
use App\Http\Controllers\RegistroController;
use App\Http\Controllers\VehiculosController;
use App\Http\Controllers\TarjetaController;
use App\Http\Controllers\MiembroController;
use App\Http\Controllers\InformaticaRegistroController;
use App\Http\Controllers\MovilRegistroController;
use App\Http\Controllers\TarjetaRegistroController;
use App\Http\Controllers\VehiculoRegistroController;
use App\Http\Controllers\RoleController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('vendor/adminlte/welcome');
});

/*Route::get('/users', function () {
    return view('vendor/adminlte/users');
});

Route::get('/departamentos', function () {
    return view('vendor/adminlte/departamentos');
});

Route::get('/materiales', function () {
    return view('vendor/adminlte/materiales');
});*/
Route::get('/roles', [RoleController::class, 'getIndex']); 
Route::get('/users', [UserController::class, 'getIndex']); 

Route::get('/departamentos', [DepartamentoController::class, 'getIndex']); 


Route::get('/miembros', [MiembroController::class, 'getIndex']); 


Route::get('/informaticas', [InformaticaController::class, 'getIndex']); 

Route::get('/moviles', [MovilController::class, 'getIndex']);

Route::get('/vehiculos', [VehiculosController::class, 'getIndex']);

Route::get('/tarjetas', [TarjetaController::class, 'getIndex']);

Route::get('/iregistro', [InformaticaRegistroController::class, 'getIndex']); 

Route::get('/mregistro', [MovilRegistroController::class, 'getIndex']);

Route::get('/vregistro', [VehiculoRegistroController::class, 'getIndex']);

Route::get('/tregistro', [TarjetaRegistroController::class, 'getIndex']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])/*->name('home')*/;

Route::get('/login', function () {
    return view('auth/login');
});
Route::get('/logout', function () {
    return'Logout usuario';
});
Route::get('/reset', function () {
    return view('auth/passwords/reset');
});
Route::get('/verify', function () {
    return view('auth/verify');
});

Route::get('empleado', [UserController::class, 'getIndex']); 
 
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes(['verify' => 'true']);
//-------------------------------------------------------------------------------------------------
//Ususarios
Route::group(['middleware' => 'verified'], function () {

    Route::get('/perfil', [UserController::class, 'getPerfil']); 

    Route::get('perfil/{id}', [UserController::class, 'getPerfil']); 

    Route::get('empleados/create', [UserController::class, 'getCreate']); 

    Route::get('empleados/edit/{id}', [UserController::class, 'getEdit']); 
    
    Route::get('empleados/show/{id}', [UserController::class, 'getShow']);

    
   Route::put('empleados/edit/{id}',[UserController::class,'putEdit']);

});
Route::post('empleados/create',[UserController::class,'postCreate']);

Route::delete('/empleados/delete/{id}',[UserController::class,'putDelete']);
//-------------------------------------------------------------------------------------------------
//Departamentos
Route::group(['middleware' => 'verified'], function () {


    Route::get('departamentos/create', [DepartamentoController::class, 'getCreate']); 

    Route::get('departamentos/edit/{id}', [DepartamentoController::class, 'getEdit']); 
    
    Route::get('departamentos/show/{id}', [DepartamentoController::class, 'getShow']);

    
   Route::put('departamentos/edit/{id}',[DepartamentoController::class,'putEdit']);

});
Route::post('departamentos/create',[DepartamentoController::class,'postCreate']);

Route::delete('/departamentos/delete/{id}',[DepartamentoController::class,'putDelete']);



//-------------------------------------------------------------------------------------------------
//Movil
Route::group(['middleware' => 'verified'], function () {


    Route::get('moviles/create', [MovilController::class, 'getCreate']); 

    Route::get('moviles/edit/{id}', [MovilController::class, 'getEdit']); 
    
    Route::get('moviles/show/{id}', [MovilController::class, 'getShow']);

    
   Route::put('moviles/edit/{id}',[MovilController::class,'putEdit']);

});
Route::post('moviles/create',[MovilController::class,'postCreate']);

Route::delete('/moviles/delete/{id}',[MovilController::class,'putDelete']);

//-------------------------------------------------------------------------------------------------
//Material informatico
Route::group(['middleware' => 'verified'], function () {


    Route::get('informaticas/create', [InformaticaController::class, 'getCreate']); 

    Route::get('informaticas/edit/{id}', [InformaticaController::class, 'getEdit']); 
    
    Route::get('informaticas/show/{id}', [InformaticaController::class, 'getShow']);

    
   Route::put('informaticas/edit/{id}',[InformaticaController::class,'putEdit']);

});
Route::post('informaticas/create',[InformaticaController::class,'postCreate']);

Route::delete('/informaticas/delete/{id}',[InformaticaController::class,'putDelete']);

//-------------------------------------------------------------------------------------------------
//Tarjeta de acceso
Route::group(['middleware' => 'verified'], function () {


    Route::get('tarjetas/create', [TarjetaController::class, 'getCreate']); 

    Route::get('tarjetas/edit/{id}', [TarjetaController::class, 'getEdit']); 
    
    Route::get('tarjetas/show/{id}', [TarjetaController::class, 'getShow']);

    
   Route::put('tarjetas/edit/{id}',[TarjetaController::class,'putEdit']);

});
Route::post('tarjetas/create',[TarjetaController::class,'postCreate']);

Route::delete('/tarjetas/delete/{id}',[TarjetaController::class,'putDelete']);

//-------------------------------------------------------------------------------------------------
//Vehiculos
Route::group(['middleware' => 'verified'], function () {


    Route::get('vehiculos/create', [VehiculosController::class, 'getCreate']); 

    Route::get('vehiculos/edit/{id}', [VehiculosController::class, 'getEdit']); 
    
    Route::get('vehiculos/show/{id}', [VehiculosController::class, 'getShow']);

    
   Route::put('vehiculos/edit/{id}',[VehiculosController::class,'putEdit']);

});
Route::post('vehiculos/create',[VehiculosController::class,'postCreate']);

Route::delete('/vehiculos/delete/{id}',[VehiculosController::class,'putDelete']);

//-------------------------------------------------------------------------------------------------
//Registros
Route::group(['middleware' => 'verified'], function () {


    Route::get('registros/create', [RegistroController::class, 'getCreate']); 

    Route::get('registros/edit/{id}', [RegistroController::class, 'getEdit']); 
    
    Route::get('registros/show/{id}', [RegistroController::class, 'getShow']);

    
   Route::put('registros/edit/{id}',[RegistroController::class,'putEdit']);

});
Route::post('registros/create',[RegistroController::class,'postCreate']);

Route::delete('/registros/delete/{id}',[RegistroController::class,'putDelete']);

//Miembros
Route::group(['middleware' => 'verified'], function () {


    Route::get('miembros/create', [MiembroController::class, 'getCreate']); 

    Route::get('miembros/edit/{id}', [MiembroController::class, 'getEdit']); 
    
    Route::get('miembros/show/{id}', [MiembroController::class, 'getShow']);

    
   Route::put('miembros/edit/{id}',[MiembroController::class,'putEdit']);

});
Route::post('miembros/create',[MiembroController::class,'postCreate']);

Route::delete('/miembros/delete/{id}',[MiembroController::class,'putDelete']);

//Iregistro
Route::group(['middleware' => 'verified'], function () {


    Route::get('registrosinformaticas/create', [InformaticaRegistroController::class, 'getCreate']); 

    Route::get('registrosinformaticas/edit/{id}', [InformaticaRegistroController::class, 'getEdit']); 
    
    Route::get('registrosinformaticas/show/{id}', [InformaticaRegistroController::class, 'getShow']);

    
   Route::put('registrosinformaticas/edit/{id}',[InformaticaRegistroController::class,'putEdit']);

});
Route::post('registrosinformaticas/create',[InformaticaRegistroController::class,'postCreate']);

Route::delete('/registrosinformaticas/delete/{id}',[InformaticaRegistroController::class,'putDelete']);
//Tregisto
Route::group(['middleware' => 'verified'], function () {


    Route::get('registrostarjetas/create', [TarjetaRegistroController::class, 'getCreate']); 

    Route::get('registrostarjetas/edit/{id}', [TarjetaRegistroController::class, 'getEdit']); 
    
    Route::get('registrostarjetas/show/{id}', [TarjetaRegistroController::class, 'getShow']);

    
   Route::put('registrostarjetas/edit/{id}',[TarjetaRegistroController::class,'putEdit']);

});
Route::post('registrostarjetas/create',[TarjetaRegistroController::class,'postCreate']);

Route::delete('/registrostarjetas/delete/{id}',[TarjetaRegistroController::class,'putDelete']);

//Vregistro
Route::group(['middleware' => 'verified'], function () {


    Route::get('registrosvehiculos/create', [VehiculoRegistroController::class, 'getCreate']); 

    Route::get('registrosvehiculos/edit/{id}', [VehiculoRegistroController::class, 'getEdit']); 
    
    Route::get('registrosvehiculos/show/{id}', [VehiculoRegistroController::class, 'getShow']);

    
   Route::put('registrosvehiculos/edit/{id}',[VehiculoRegistroController::class,'putEdit']);

});
Route::post('registrosvehiculos/create',[VehiculoRegistroController::class,'postCreate']);

Route::delete('/registrosvehiculos/delete/{id}',[VehiculoRegistroController::class,'putDelete']);
//Mregistro
Route::group(['middleware' => 'verified'], function () {


    Route::get('registrosmoviles/create', [MovilRegistroController::class, 'getCreate']); 

    Route::get('registrosmoviles/edit/{id}', [MovilRegistroController::class, 'getEdit']); 
    
    Route::get('registrosmoviles/show/{id}', [MovilRegistroController::class, 'getShow']);

    
   Route::put('registrosmoviles/edit/{id}',[MovilRegistroController::class,'putEdit']);

});
Route::post('registrosmoviles/create',[MovilRegistroController::class,'postCreate']);

Route::delete('/registrosmoviles/delete/{id}',[MovilRegistroController::class,'putDelete']);

//Roles
Route::group(['middleware' => ['auth']], function() {
Route::resource('roles', RoleController::class);
Route::resource('users', UserController::class);
});