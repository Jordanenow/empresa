<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Gate;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (request()->server->has('HTTP_X_ORIGINAL_HOST')) {

            request()->server->set('HTTP_HOST', request()->server->get('HTTP_X_ORIGINAL_HOST'));
            
            request()->headers->set('HOST', request()->server->get('HTTP_X_ORIGINAL_HOST'));
            
            }
            Paginator::useBootstrap();

           

            Gate::define('admin', function(User $user){
                if($user->getRoleNames()[0]=='Admin'){
                    return true;
                }return false;
            });
            Gate::define('supervisor', function(User $user){
                if($user->getRoleNames()[0]=='Supervisor'){
                    return true;
                }return false;
            });
    }
}
