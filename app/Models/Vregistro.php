<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vregistro extends Model
{
    use HasFactory;
    public $table = "vehiculosregistrados";
}
