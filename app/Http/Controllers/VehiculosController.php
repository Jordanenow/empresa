<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Vehiculo;

class VehiculosController extends Controller
{
    public function getIndex()
    {

        $Vehiculo = DB::table('vehiculos')->paginate(10);

        return view('vendor/adminlte/vehiculos/index', ['vehiculos' => $Vehiculo]);
    }

    public function getShow($id)
    {

        $Vehiculo = Vehiculo::findOrFail($id);

        return view('vendor/adminlte/vehiculos/show', ['Vehiculos' => $Vehiculo]);
    }
   
    public function getCreate()
    {

        return view('vendor/adminlte/vehiculos/create');
    }
    public function getEdit($id)
    {
        $Vehiculo = Vehiculo::findOrFail($id);

        return view('vendor/adminlte/vehiculos/edit', ['vehiculo' => $Vehiculo]);
    }

    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear 

        $vehiculo = new Vehiculo();
        $vehiculo->modelo = $request->input('modelo');
        $vehiculo->matricula = $request->input('matricula');
        $vehiculo->save();

        return redirect('/vehiculos');
    }

        public function putEdit(Request $request, $id)
    {

        //----------------------------------------------------------------------------------------
        //Editar 
        $vehiculo = Vehiculo::findorfail($id);
        $vehiculo->modelo = $request->input('modelo');
        $vehiculo->matricula = $request->input('matricula');
        $vehiculo->save();
        return redirect('/vehiculos');

    }
    public function putDelete(Request $request, $id)
    {
        //----------------------------------------------------------------------------------------
        //Borrar Departamento
        $borrar =  Vehiculo::findorfail($id);
        $borrar->delete();
        return redirect('/vehiculos');
    }
}
