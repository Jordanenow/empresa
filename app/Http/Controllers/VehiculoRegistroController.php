<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Vregistro;
use App\Models\Vehiculo;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;

class VehiculoRegistroController extends Controller
{
    public function getShow($id)
    {
        $Vregistro =DB::table("vehiculosregistrados")
        ->select()->where('id','=',$id)->get();
        //dd($Miembro);

        return view('vendor/adminlte/registrosvehiculos/show', ['Vregistro' => $Vregistro[0]]);
    }
    public function getIndex()
    {
        
        $Vregistro =DB::table("vehiculosregistrados")
            ->join('users','vehiculosregistrados.user_id','=','users.id')
            ->join('vehiculos','vehiculosregistrados.vehiculo_id','=','vehiculos.id')
            ->select('vehiculos.*','users.name','vehiculosregistrados.*')
            ->paginate(10);
       
           // dd($Miembro);
        return view('vendor/adminlte/registrosvehiculos/index', ['vregistros' => $Vregistro]);
    }
    public function getCreate()
    {
        $users = User::all();
        $vehiculos = Vehiculo::all();
        return view('vendor/adminlte/registrosvehiculos/create',["vehiculos"=> $vehiculos],["users"=> $users]);
    }
    public function getEdit($id)
     {
         $Vregistro = Vregistro::findOrFail($id);

        return view('vendor/adminlte/registrosvehiculos/edit', ['vregistro' => $Vregistro]);
    }
    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear 

        $vregistro = new Vregistro();
        $vregistro-> user_id = $request-> input('users');
        $vregistro-> vehiculo_id = $request-> input('vehiculos');

        $vregistro->save();

        return redirect('vregistro');
    }

     public function putEdit(Request $request, $id)
    {

       //----------------------------------------------------------------------------------------
       //Editar 
        $vregistro = Vregistro::findorfail($id);
        $vregistro->nombre = $request->input('nombre');
        $vregistro->horario = $request->input('horario');
        $vregistro->save();
         return redirect('miembro');
         }
    
    public function putDelete(Request $request, $id)
    {

        $borrar =  Vregistro::findorfail($id);
        $borrar->delete();
        return redirect('miembro');
    }
}
