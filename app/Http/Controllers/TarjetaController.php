<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Tarjeta;

class TarjetaController extends Controller
{
    public function getIndex()
    {

        $Tarjeta = DB::table('tarjetasaccesos')->paginate(10);

        return view('vendor/adminlte/tarjetas/index', ['tarjetas' => $Tarjeta]);
    }

    public function getShow($id)
    {

        $Tarjeta = Tarjeta::findOrFail($id);

        return view('vendor/adminlte/tarjetas/show', ['Tarjeta' => $Tarjeta]);
    }
   
    public function getCreate()
    {

        return view('vendor/adminlte/tarjetas/create');
    }
    public function getEdit($id)
    {
        $Tarjeta = Tarjeta::findOrFail($id);

        return view('vendor/adminlte/tarjetas/edit', ['departamento' => $Tarjeta]);
    }

    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear 

        $tarjeta = new Tarjeta();
        $tarjeta->codigo = $request->input('codigo');
        $tarjeta->save();

        return redirect('/tarjetas');
    }

        public function putEdit(Request $request, $id)
    {

        //----------------------------------------------------------------------------------------
        //Editar 
        $tarjeta = Tarjeta::findorfail($id);
        $tarjeta->codigo = $request->input('codigo');
        $tarjeta->save();
        return redirect('/tarjetas');

    }
    public function putDelete(Request $request, $id)
    {
        //----------------------------------------------------------------------------------------
        //Borrar 
        $borrar =  Tarjeta::findorfail($id);
        $borrar->delete();
        return redirect('/tarjetas');
    }
}
