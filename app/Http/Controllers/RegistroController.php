<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Registro;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;

class RegistroController extends Controller
{
    public function getShow($id)
    {
        $Registro = Registro::findOrFail($id);

        return view('vendor/adminlte/registro/show', ['Registro' => $Registro]);
    }
    public function getIndex()
    {
        
        $registro =DB::table("registros")
            ->join('users','registros.user_id','=','users.id')
            ->join('materiales','registros.material_id','=','materiales.id')
            ->select('registros.*','users.name','materiales.nombre')
            ->paginate(10);
       

        return view('vendor/adminlte/registros/index', ['registros' => $registro]);
    }
    public function getCreate()
    {
         //Para coger las zonas
        $materiales = Material::all();
        return view('vendor/adminlte/registros/create',["materiales"=> $materiales]);
    }
    public function getEdit($id)
     {
         $Registro = Registro::findOrFail($id);

        return view('vendor/adminlte/registros/edit', ['registro' => $Registro]);
    }
    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear Registro

        $registro = new Registro();
        //Para coger la ide del usuario actual
        $id = Auth::user()->id;
        $nombre = DB::table("users")->where("id", $id)->select("name")->get();
    
       

       
        $registro-> user_id = $id;
        $registro-> materiale_id = $request-> input('materiales');

        $registro->save();

        return redirect('registro');
    }

     public function putEdit(Request $request, $id)
    {

       //----------------------------------------------------------------------------------------
       //Editar Registro
        $registro = Registro::findorfail($id);
        $registro->nombre = $request->input('nombre');
        $registro->horario = $request->input('horario');
        $registro->save();
         return redirect('registro');
         }
    
    public function putDelete(Request $request, $id)
    {

        $borrar =  Registro::findorfail($id);
        $borrar->delete();
        return redirect('registro');
    }
}
