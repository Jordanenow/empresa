<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Informatica;

class InformaticaController extends Controller
{
   
    
    public function getIndex()
    {

        $Informatica = DB::table('materialesinformaticos')->paginate(10);

        return view('vendor/adminlte/informaticas/index', ['informaticas' => $Informatica]);
    }

    public function getShow($id)
    {

        $Informatica = Informatica::findOrFail($id);

        return view('vendor/adminlte/informaticas/show', ['Informatica' => $Informatica]);
    }
   
    public function getCreate()
    {

        return view('vendor/adminlte/informaticas/create');
    }
    public function getEdit($id)
    {
        $Informatica = Informatica::findOrFail($id);

        return view('vendor/adminlte/informaticas/edit', ['informatica' => $Informatica]);
    }

    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear Departamento

        $informatica = new Informatica();
        $informatica->nombre = $request->input('nombre');
        $informatica->MAC = $request->input('MAC');

        $informatica->save();

        return redirect('/informaticas');
    }

        public function putEdit(Request $request, $id)
    {

        //----------------------------------------------------------------------------------------
        //Editar Departamento
        $informatica = Informatica::findorfail($id);
        $informatica->nombre = $request->input('nombre');
        $informatica->save();
        return redirect('/informaticas');

    }
    public function putDelete(Request $request, $id)
    {
        //----------------------------------------------------------------------------------------
        //Borrar Departamento
        $borrar =  Informatica::findorfail($id);
        $borrar->delete();
        return redirect('/informaticas');
    }
}
