<?php

namespace App\Http\Controllers;
use App\Models\Departamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DepartamentoController extends Controller
{
    
    public function getIndex()
    {

        $Departamento = DB::table('departamentos')->paginate(10);

        return view('vendor/adminlte/departamentos', ['departamentos' => $Departamento]);
    }

    public function getShow($id)
    {

        $Departamento = Departamento::findOrFail($id);

        return view('vendor/adminlte/departamentos/show', ['Departamento' => $Departamento]);
    }
   
    public function getCreate()
    {

        return view('vendor/adminlte/departamentos/create');
    }
    public function getEdit($id)
    {
        $Departamento = Departamento::findOrFail($id);

        return view('vendor/adminlte/departamentos/edit', ['departamento' => $Departamento]);
    }

    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear Departamento

        $departamento = new Departamento();
        $departamento->nombre = $request->input('nombre');
        $departamento->save();

        return redirect('/departamentos');
    }

        public function putEdit(Request $request, $id)
    {

        //----------------------------------------------------------------------------------------
        //Editar Departamento
        $departamento = Departamento::findorfail($id);
        $departamento->nombre = $request->input('nombre');
        $departamento->save();
        return redirect('/departamentos');

    }
    public function putDelete(Request $request, $id)
    {
        //----------------------------------------------------------------------------------------
        //Borrar Departamento
        $borrar =  Departamento::findorfail($id);
        $borrar->delete();
        return redirect('/departamentos');
    }
}
