<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Mregistro;
use App\Models\Movil;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;

class MovilRegistroController extends Controller
{
    public function getShow($id)
    {
        $Mregistro =DB::table("movilesregistros")
        ->select()->where('id','=',$id)->get();
        

        return view('vendor/adminlte/registrosmoviles/show', ['Mregistro' => $Mregistro[0]]);
    }
    public function getIndex()
    {
        
        $Mregistro =DB::table("movilesregistros")
            ->join('users','movilesregistros.user_id','=','users.id')
            ->join('moviles','movilesregistros.movil_id','=','moviles.id')
            ->select('moviles.*','users.name','movilesregistros.*')
            ->paginate(10);
       
           // dd($Miembro);
        return view('vendor/adminlte/registrosmoviles/index', ['mregistros' => $Mregistro]);
    }
    public function getCreate()
    {
        $users = User::all();
        $moviles = Movil::all();
        return view('vendor/adminlte/registrosmoviles/create',["moviles"=> $moviles],["users"=> $users]);
    }
    public function getEdit($id)
     {
         $Mregistro = Mregistro::findOrFail($id);

        return view('vendor/adminlte/registrosmoviles/edit', ['mregistro' => $Mregistro]);
    }
    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear Miembro

        $mregistro = new Mregistro();
     
       

        $mregistro-> user_id = $request-> input('users');
        $mregistro-> movil_id = $request-> input('moviles');

        $mregistro->save();

        return redirect('mregistro');
    }

     public function putEdit(Request $request, $id)
    {

       //----------------------------------------------------------------------------------------
       //Editar 
        $mregistro = Mregistro::findorfail($id);
        $mregistro->nombre = $request->input('nombre');
        $mregistro->horario = $request->input('horario');
        $mregistro->save();
         return redirect('mregistro');
         }
    
    public function putDelete(Request $request, $id)
    {

        $borrar =  Mregistro::findorfail($id);
        $borrar->delete();
        return redirect('mregistro');
    }
}
