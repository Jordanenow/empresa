<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tregistro;
use App\Models\Tarjeta;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;

class TarjetaRegistroController extends Controller
{
    public function getShow($id)
    {
        $Tregistro =DB::table("tarjetasregistradas")
        ->select()->where('id','=',$id)->get();
        //dd($Miembro);

        return view('vendor/adminlte/registrostarjetas/show', ['Tregistro' => $Tregistro[0]]);
    }
    public function getIndex()
    {
        
        $Tregistro =DB::table("tarjetasregistradas")
            ->join('users','tarjetasregistradas.user_id','=','users.id')
            ->join('tarjetasaccesos','tarjetasregistradas.tarjeta_id','=','tarjetasaccesos.id')
            ->select('tarjetasaccesos.*','users.name','tarjetasregistradas.*')
            ->paginate(10);
       
         //dd($Tregistro);
        return view('vendor/adminlte/registrostarjetas/index', ['tregistros' => $Tregistro]);
    }
    public function getCreate()
    {
        $users = User::all();
        $tarjetas = Tarjeta::all();
        return view('vendor/adminlte/registrostarjetas/create',["tarjetas"=> $tarjetas],["users"=> $users]);
    }
    public function getEdit($id)
     {
         $Tregistro = Tregistro::findOrFail($id);

        return view('vendor/adminlte/registrostarjetas/edit', ['tregistro' => $Tregistro]);
    }
    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear 

        $tregistro = new Tregistro();
        $tregistro-> user_id = $request-> input('users');
        $tregistro-> tarjeta_id = $request-> input('tarjetas');

        $tregistro->save();

        return redirect('tarjetas');
    }

     public function putEdit(Request $request, $id)
    {

       //----------------------------------------------------------------------------------------
       //Editar Registro
        $tregistro = Tregistro::findorfail($id);
        $tregistro->nombre = $request->input('nombre');
        $tregistro->horario = $request->input('horario');
        $tregistro->save();
         return redirect('tarjetas');
         }
    
    public function putDelete(Request $request, $id)
    {

        $borrar =  Tregistro::findorfail($id);
        $borrar->delete();
        return redirect('tarjetas');
    }
}
