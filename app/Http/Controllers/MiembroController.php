<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Miembro;
use App\Models\Departamento;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;

class MiembroController extends Controller
{
    public function getShow($id)
    {
        $Miembro =DB::table("miembros")
        ->select()->where('id','=',$id)->get();
        //dd($Miembro);

        return view('vendor/adminlte/miembros/show', ['Miembro' => $Miembro[0]]);
    }
    public function getIndex()
    {
        
        $Miembro =DB::table("miembros")
            ->join('users','miembros.user_id','=','users.id')
            ->join('departamentos','miembros.departamento_id','=','departamentos.id')
            ->select('departamentos.*','users.name','miembros.*')
            ->paginate(10);
       
        
        return view('vendor/adminlte/miembros/index', ['miembros' => $Miembro]);
    }
    public function getCreate()
    {
        $users = User::all();

        $departamentos = Departamento::all();
        return view('vendor/adminlte/miembros/create',["departamentos"=> $departamentos],["users"=> $users]);
    }
    public function getEdit($id)
     {
         $Miembro = Miembro::findOrFail($id);

        return view('vendor/adminlte/miembros/edit', ['miembro' => $Miembro]);
    }
    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear Miembro

        $miembro = new Miembro();
        //Para coger la ide del usuario actual
       
       

        //$registro->hora = new DateTime();
        $miembro-> user_id = $request-> input('users');
        $miembro-> departamento_id = $request-> input('departamentos');

        $miembro->save();

        return redirect('miembros');
    }

     public function putEdit(Request $request, $id)
    {

       //----------------------------------------------------------------------------------------
       //Editar Registro
        $miembro = Miembro::findorfail($id);
        $miembro->nombre = $request->input('nombre');
        $miembro->horario = $request->input('horario');
        $miembro->save();
         return redirect('miembros');
         }
    
    public function putDelete(Request $request, $id)
    {

        $borrar =  Miembro::findorfail($id);
        $borrar->delete();
        return redirect('miembros');
    }
}
