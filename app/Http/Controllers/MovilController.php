<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Movil;
class MovilController extends Controller
{
    public function getIndex()
    {

        $Movil = DB::table('moviles')->paginate(10);

        return view('vendor/adminlte/moviles/index', ['moviles' => $Movil]);
    }

    public function getShow($id)
    {

        $Movil = Movil::findOrFail($id);

        return view('vendor/adminlte/moviles/show', ['Movil' => $Movil]);
    }
   
    public function getCreate()
    {

        return view('vendor/adminlte/moviles/create');
    }
    public function getEdit($id)
    {
        $Movil = Movil::findOrFail($id);

        return view('vendor/adminlte/moviles/edit', ['movil' => $Movil]);
    }

    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear 

        $movil = new Movil();
        $movil->numero = $request->input('numero');
        $movil->marca = $request->input('marca');
        $movil->cuenta_google = $request->input('cuenta_google');
        $movil->save();

        return redirect('/moviles');
    }

        public function putEdit(Request $request, $id)
    {

        //----------------------------------------------------------------------------------------
        //Editar 
        $movil = Movil::findorfail($id);
        $movil->numero = $request->input('numero');
        $movil->marca = $request->input('marca');
        $movil->cuenta_google = $request->input('cuenta_google');
        $movil->save();
        return redirect('/moviles');

    }
    public function putDelete(Request $request, $id)
    {
        //----------------------------------------------------------------------------------------
        //Borrar 
        $borrar =  Movil::findorfail($id);
        $borrar->delete();
        return redirect('/moviles');
    }
}
