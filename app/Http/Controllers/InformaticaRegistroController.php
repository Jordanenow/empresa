<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Iregistro;
use App\Models\Informatica;
use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\Auth;

class InformaticaRegistroController extends Controller
{
    public function getShow($id)
    {
        $Iregistro = Iregistro::findOrFail($id);

        return view('vendor/adminlte/registrosinformaticas/show', ['Iregistro' => $Iregistro]);
    }
    public function getIndex()
    {
        
        $Iregistro =DB::table("informaticasregistradas")
            ->join('users','informaticasregistradas.user_id','=','users.id')
            ->join('materialesinformaticos','informaticasregistradas.informatica_id','=','materialesinformaticos.id')
            ->select('materialesinformaticos.*','users.name','informaticasregistradas.*')
            ->paginate(10);

        return view('vendor/adminlte/registrosinformaticas/index', ['iregistros' => $Iregistro]);
    }
    public function getCreate()
    {
        $users = User::all();
        $informaticas = Informatica::all();
        return view('vendor/adminlte/registrosinformaticas/create',["informaticas"=> $informaticas],["users"=> $users]);
    }
    public function getEdit($id)
     {
         $Iregistro = Iregistro::findOrFail($id);

        return view('vendor/adminlte/registrosinformaticas/edit', ['iregistro' => $Iregistro]);
    }
    public function postCreate(Request $request)
    {

        //---------------------------------------------------------------------------------------- 
        // Crear 

        $iregistro = new Iregistro();
        $iregistro-> user_id = $request-> input('users');
        $iregistro-> informatica_id = $request-> input('informaticas');

        $iregistro->save();

        return redirect('iregistro');
    }

     public function putEdit(Request $request, $id)
    {

       //----------------------------------------------------------------------------------------
       //Editar 
        $iregistro = Iregistro::findorfail($id);
        $iregistro->nombre = $request->input('nombre');
        $iregistro->horario = $request->input('horario');
        $iregistro->save();
         return redirect('iregistro');
         }
    
    public function putDelete(Request $request, $id)
    {

        $borrar = Iregistro::findorfail($id);
        $borrar->delete();
        return redirect('iregistro');
    }
}
